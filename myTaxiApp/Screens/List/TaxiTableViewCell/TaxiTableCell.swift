//
//  TaxiTableCell
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit

class TaxiTableCell: UITableViewCell {
    
    //MARK: - Private properties
    fileprivate var car: Car?
    fileprivate var viewModel: AddressViewModel?
    //MARK: IBOutlets
    @IBOutlet weak var availabilityImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK: - Public methods
    func setup(with car: Car) {
        self.car = car
        if viewModel == nil {
            viewModel = AddressViewModel(coordinates: car.coordinates)
        }
        setupUI()
    }
}

//MARK: - Private methods
fileprivate extension TaxiTableCell {
    
    func setupUI() {
        guard let car = car else { return }
        setupAddress()
        let imageName = car.isActive ? "car_green" : "car_red"
        availabilityImageView.image = UIImage(named: imageName)

    }
    
    func setupAddress() {
        guard let viewModel = viewModel else { return }
        
        viewModel.getFullAddress { [weak self] (addressString) in
            guard let `self` = self else { return }
            
            if let addressString = addressString {
                self.addressLabel.text = addressString
            }
        }
    }
}
