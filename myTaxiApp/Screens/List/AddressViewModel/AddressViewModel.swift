//
//  TaxiTableCellModel.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/1/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit

class AddressViewModel: NSObject {
    
    //Mark: - Private properties
    private var coordinates: CoordinateSet
    private var addressString: String?
    private var locationManager: LocationManager = LocationManager()
    
    //Mark: - Init
    init(coordinates: CoordinateSet) {
        self.coordinates = coordinates
    }
    
    //Mark: - Public methods
    func getFullAddress(completionBlock: @escaping (String?) -> Void) {
        if addressString != nil {
            completionBlock(addressString)
        } else {
            locationManager.addressFromCoordinates((coordinates.latitude, coordinates.longitude),
                                                completionHandler: { [weak self] (addressString) in
                guard let `self` = self else { return }
                self.addressString = addressString ?? ""
                completionBlock(addressString)
            })
        }
    }
}
