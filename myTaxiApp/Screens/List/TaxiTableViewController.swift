//
//  TaxiTableViewController.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

class TaxiTableViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        }
    }
    //MARK: Public properties
    var carsArray = [Car]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource
extension TaxiTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TaxiTableCell.self), for: indexPath) as! TaxiTableCell
        let car = carsArray[indexPath.row]
        cell.setup(with: car)
        
        return cell
    }
}
