//
//  MapAnnotation.m
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import "MapAnnotation.h"
#import <myTaxiApp-Swift.h>

@interface MapAnnotation()

@property (assign, nonatomic, readwrite) double carId;
@property (copy, nonatomic) NSString *title;
@end

@implementation MapAnnotation

- (instancetype)initWithCar:(Car *)car {
    if (self = [super init]) {
        self.coordinate = [car coordinate2D];
        self.carId = car.identifier;
        self.title = [NSString stringWithFormat: @"Number %@",[NSNumber numberWithDouble:car.identifier]];
    }
    return self;
}

@end
