//
//  MapAnnotation.h
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Car;

NS_ASSUME_NONNULL_BEGIN

@interface MapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (assign, nonatomic, readonly) double carId;

- (instancetype)initWithCar:(Car *)car;

@end

NS_ASSUME_NONNULL_END
