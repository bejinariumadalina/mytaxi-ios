//
//  MapViewController.h
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/1/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol MapViewControllerDelegate <NSObject>

-(void)fetchCarsInBoundsWithCoordinate1: (CLLocationCoordinate2D)coordinate1 coordinate2:(CLLocationCoordinate2D)coordinate2;

@end

NS_ASSUME_NONNULL_BEGIN
@interface MapViewController : UIViewController

@property (strong, nonatomic, nonnull) NSArray *carsArray;
@property (weak, nonatomic) id <MapViewControllerDelegate> delegate;

@end
NS_ASSUME_NONNULL_END
