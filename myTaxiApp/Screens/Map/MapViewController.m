//
//  MapViewController.m
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/1/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import "MapViewController.h"
#import <myTaxiApp-Swift.h>
#import <MapKit/MapKit.h>
#import "MapAnnotation.h"
#import "NSArray+Utils.h"
#import "MapViewModel.h"

@interface MapViewController () <MKMapViewDelegate>

#pragma mark Properties
@property (strong, nonatomic, nonnull) MapViewModel *viewModel;
 @property(strong, nonatomic) NSTimer *timer;
@property (nonatomic, assign) BOOL nextRegionChangeIsFromUserInteraction;


#pragma mark IBOutlets
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLocationLogic];
}

#pragma mark - Setup

- (void)setupLocationLogic {
    self.viewModel = [MapViewModel new];
    [LocationManager.sharedInstance startUpdatingLocation];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(refreshPins) userInfo:nil repeats:YES];
}

- (void)centerMapOn: (CLLocationCoordinate2D)location {
    CLLocationDistance distance = 2000;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, distance, distance);
    [self.mapView setRegion:region];
}

#pragma mark - Custom Setters

- (void)setCarsArray:(NSArray *)carsArray {
    NSArray *oldLocations = _carsArray;
    NSArray *newLocations = carsArray;
    _carsArray = carsArray;
    
    if (oldLocations == nil) {
        [self addAnnotationCars: newLocations];
    } else {
        NSArray *arrayToUpdate = [newLocations intersectWithArray:oldLocations];
        NSArray *arrayToAdd = [newLocations substractArray:oldLocations];
        NSArray *arrayToRemove = [oldLocations substractArray:newLocations];
        
        for (MapAnnotation *annotation in self.mapView.annotations) {
            if ([annotation isKindOfClass:[MKUserLocation class]]) { continue; }
            [self verifyAnnotation:annotation inRequiresDeleteArray:arrayToRemove];
            [self verifyAnnotation:annotation inRequiresUpdateArray:arrayToUpdate];
        }
        [self addAnnotationCars: arrayToAdd];
    }
}

#pragma mark - Annotations actions

- (void)addAnnotationCars:(NSArray *)carsArray {
    for (Car *item in carsArray) {
        MapAnnotation *annotationPoint = [[MapAnnotation alloc] initWithCar:item];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView addAnnotation:annotationPoint];
        });
    }
}

- (void)verifyAnnotation:(MapAnnotation *)annotation inRequiresDeleteArray:(NSArray *) deleteArray {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %lf", annotation.carId];
    NSArray *filteredDeleteArray = [deleteArray filteredArrayUsingPredicate:predicate];
    if (filteredDeleteArray.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView removeAnnotation:annotation];
        });
    }
}

- (void)verifyAnnotation:(MapAnnotation *)annotation inRequiresUpdateArray:(NSArray *) updateArray {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %lf", annotation.carId];
    NSArray *filteredUpdateArray = [updateArray filteredArrayUsingPredicate:predicate];
    
    if (filteredUpdateArray.count > 0) {
        Car *car = (Car*)filteredUpdateArray.firstObject;
        CLLocationCoordinate2D newLocation = [car coordinate2D];
        float angle = [self.viewModel angleFromCoordinate:annotation.coordinate toCoordinate: newLocation];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:2 animations:^{
                 annotation.coordinate = newLocation;
                 MKAnnotationView *annotationView = [self.mapView viewForAnnotation:annotation];
                 annotationView.transform = CGAffineTransformMakeRotation(angle);
             }];
        });
    }
}

#pragma mark - Refresh

- (void)refreshPins {
    
    MKMapRect mapRect = self.mapView.visibleMapRect;
    CLLocationCoordinate2D swCoordinate = [self.viewModel getCoordinateFromMapRectanglePoint: mapRect.origin.x y: MKMapRectGetMaxY(mapRect)];
    CLLocationCoordinate2D neCoordinate = [self.viewModel getCoordinateFromMapRectanglePoint: MKMapRectGetMaxX(mapRect) y: mapRect.origin.y];
    
    if ([self.delegate respondsToSelector:@selector(fetchCarsInBoundsWithCoordinate1:coordinate2:)]) {
        [self.delegate fetchCarsInBoundsWithCoordinate1:neCoordinate coordinate2:swCoordinate];
    }
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)sender viewForAnnotation:(id < MKAnnotation >)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    NSString *reuseIdentifier = @"CarPin";
    MKAnnotationView *pinView = (MKAnnotationView *)[sender
                                                   dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
    if (pinView == nil) {
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
        pinView.canShowCallout = YES;
    } else {
        pinView.annotation = annotation;
    }
    pinView.image = [UIImage imageNamed : @"pin_car"];

    return pinView;
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    UIView* view = mapView.subviews.firstObject;
    for(UIGestureRecognizer* recognizer in view.gestureRecognizers) {
        if (recognizer.state == UIGestureRecognizerStateBegan ||
           recognizer.state == UIGestureRecognizerStateEnded) {
            self.nextRegionChangeIsFromUserInteraction = YES;
            break;
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self centerMapOn:userLocation.coordinate];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if(self.nextRegionChangeIsFromUserInteraction) {
        self.nextRegionChangeIsFromUserInteraction = NO;
        [self refreshPins];
    }
}

@end
