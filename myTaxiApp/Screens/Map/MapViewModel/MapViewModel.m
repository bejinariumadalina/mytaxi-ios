//
//  MapViewModel.m
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import "MapViewModel.h"

@implementation MapViewModel

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first
                toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)
        return angle;
    else if (deltaLongitude < 0)
        return angle + M_PI;
    else if (deltaLatitude < 0)
        return M_PI;
    
    return 0.0f;
}

- (CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y {
    MKMapPoint swMapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(swMapPoint);
}

@end
