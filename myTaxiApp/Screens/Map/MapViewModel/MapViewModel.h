//
//  MapViewModel.h
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapViewModel : NSObject

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first
                toCoordinate:(CLLocationCoordinate2D)second;
- (CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y;

@end

NS_ASSUME_NONNULL_END
