//
//  ViewController.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

//enum SegmentType: Int {
//    case map
//    case list
//}

class MainViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    //MARK: Private properties
    private var isMapOnScreen: Bool = false
    private lazy var listViewController: TaxiTableViewController = {
        var viewController = self.storyboard!.instantiateViewController(withIdentifier: String(describing: TaxiTableViewController.self)) as! TaxiTableViewController
        self.add(asChildViewController: viewController)

        return viewController
    }()
    private lazy var mapViewController: MapViewController = {
        var viewController = self.storyboard!.instantiateViewController(withIdentifier: String(describing: MapViewController.self)) as! MapViewController
        viewController.delegate = self
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    private var carsArray: [Car] = [Car]() {
        didSet {
            if isMapOnScreen {
                mapViewController.carsArray = carsArray
            } else {
                listViewController.carsArray = carsArray
            }
        }
    }
    
    //Mark: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupContainerScreen()
    }

    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        //TO DO SEGMENTED CONTROL INDEX
        setupContainerScreen()
    }
}

fileprivate extension MainViewController {
    //MARK: Segmented control methods
    func setupContainerScreen() {
        isMapOnScreen = !isMapOnScreen
        if isMapOnScreen {
            remove(asChildViewController: listViewController)
            add(asChildViewController: mapViewController)
        } else {
            remove(asChildViewController: mapViewController)
            add(asChildViewController: listViewController)
            listViewController.carsArray = carsArray
        }
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        DispatchQueue.main.async {
            self.addChild(viewController)
            self.containerView.addSubview(viewController.view)
            viewController.view.frame = self.containerView.bounds
            viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            viewController.didMove(toParent: self)
        }
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        DispatchQueue.main.async {
            viewController.willMove(toParent: nil)
            viewController.view.removeFromSuperview()
            viewController.removeFromParent()
        }
    }
}

extension MainViewController: MapViewControllerDelegate {
    
    func fetchCarsInBounds(withCoordinate1 coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        let bounds = (coordinate1, coordinate2)
        Network.filterTaxis(in: bounds) { (carsArray) in
            if let carsArray = carsArray {
                self.carsArray = carsArray
            }
        }
    }
}

