//
//  CLPlacemark+Address.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

extension CLPlacemark {
    
    var compactAddress: String {
        var result = ""
        if let name = name {
            result = name
        }
        if let street = thoroughfare {
            result += ", \(street)"
        }
        if let streetno = subThoroughfare {
            result += " \(streetno)"
        }
        if let city = locality {
            result += ", \(city)"
        }
        if let sublocality = subLocality {
            result += ", \(sublocality)"
        }
        if let country = country {
            result += ", \(country)"
        }
        return result
    }
}
