//
//  NSArray+Utils.h
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (Utils)

- (NSArray*)intersectWithArray:(NSArray *)array;
- (NSArray*)substractArray:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
