//
//  NSArray+Utils.m
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 10/2/18.
//  Copyright © 2018 testapp. All rights reserved.
//

#import "NSArray+Utils.h"

@implementation NSArray (Utils)

- (NSArray*)intersectWithArray:(NSArray *)array {
    
    NSMutableSet *set1 = [NSMutableSet setWithArray: self];
    NSSet *set2 = [NSSet setWithArray: array];
    [set1 intersectSet: set2];
    return [set1 allObjects];
}

- (NSArray*)substractArray:(NSArray *)array {
    
    NSMutableSet *set1 = [NSMutableSet setWithArray: self];
    NSSet *set2 = [NSSet setWithArray: array];
    [set1 minusSet: set2];
    return [set1 allObjects];
}

@end
