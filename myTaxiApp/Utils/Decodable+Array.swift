//
//  Decodable+Array.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/26/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit

extension Decodable {
    
    static func get(from json: [Any]) throws -> [Self]? {
        
        let decoder = JSONDecoder()
        let encodedData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        do {
            let objects = try decoder.decode([Self].self, from: encodedData)
            return objects
        } catch let error {
            print(error)
        }

        return nil
    }
}
