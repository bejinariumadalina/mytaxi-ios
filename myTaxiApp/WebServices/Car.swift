//
//  Taxi.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/26/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

class CoordinateSet: Decodable {
    let latitude: Double
    let longitude: Double
}

@objc class Car: NSObject, Decodable {
    @objc let identifier: Double
    let coordinates: CoordinateSet
    let isActive: Bool
    let type: String
    let heading: Double
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case coordinates = "coordinate"
        case state
        case type
        case heading
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try values.decode(Double.self, forKey: .identifier)
        let stateString = try values.decode(String.self, forKey: .state)
        isActive = stateString == "ACTIVE"
        type = try values.decode(String.self, forKey: .type)
        heading = try values.decode(Double.self, forKey: .heading)
        coordinates = try values.decode(CoordinateSet.self, forKey: .coordinates)
    }
    
    @objc func coordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
    }
    
    //MARK - Equality
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? Car {
            return object.identifier == self.identifier
        } else {
            return false
        }
    }
    
    override var hash: Int {
        return identifier.hashValue
    }
}
