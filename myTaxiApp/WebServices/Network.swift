//
//  Network.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

class Network {
    
    private static let serverUrlString = "https://poi-api.mytaxi.com/PoiService/poi/v1"
    private static let filterPath = "?p2Lat=%f&p1Lon=%f&p1Lat=%f&p2Lon=%f"
    
    private enum Endpoint {
        case filter(coordinates: (coord1: CLLocationCoordinate2D, coord2: CLLocationCoordinate2D))
        
        var path: String {
            switch self {
            case .filter(let coordinates):
                let url = serverUrlString + String(format: filterPath,
                                                   coordinates.coord1.latitude, coordinates.coord1.longitude,
                                                   coordinates.coord2.latitude, coordinates.coord2.longitude)
                return url
            default:
                return serverUrlString
            }
        }
    }
    
    private static func request(endpoint: Endpoint, completionHandler: @escaping ([String: Any]?)-> Void) {
        guard let requestUrl = URL(string:endpoint.path) else { return }
        let task = URLSession.shared.dataTask(with: requestUrl) { (data, response, error) in
            if let data = data {
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    completionHandler(jsonDict)
                }  catch let error as NSError {
                    print(error.localizedDescription)
                    completionHandler(nil)
                }
            }  else if let error = error {
                print(error.localizedDescription)
                completionHandler(nil)
            }
        }
        task.resume()
    }
}

// MARK: - Public methods
extension Network {
    static func filterTaxis(in coordinates: (coord1: CLLocationCoordinate2D, coord2: CLLocationCoordinate2D),
                            completionHandler: @escaping ([Car]?) -> Void) {
        request(endpoint: .filter(coordinates: coordinates)) { (response) in
            if let response = response,
                let jsonArray = response["poiList"] as? [Any],
                let taxisArray = try? Car.get(from: jsonArray) {
                completionHandler(taxisArray)
            } else {
                completionHandler(nil)
            }
        }
    }
}
