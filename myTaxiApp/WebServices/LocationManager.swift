//
//  LocationManager.swift
//  myTaxiApp
//
//  Created by Madalina Bejinariu on 9/27/18.
//  Copyright © 2018 testapp. All rights reserved.
//

import UIKit
import CoreLocation

@objc class LocationManager: NSObject {
    
    //MARK: Public properties
    @objc static let sharedInstance = LocationManager()
    //MARK: Private methods
    private lazy var geocoder = CLGeocoder()
    private lazy var clLocationManager = CLLocationManager()
    @objc var locationBlock : ((CLLocationCoordinate2D) -> Void)?

    //MARK: User location
    func askAuthorisation() {
        clLocationManager.requestWhenInUseAuthorization()
    }
    
    @objc func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            clLocationManager.delegate = self
            clLocationManager.desiredAccuracy = kCLLocationAccuracyBest
            clLocationManager.startUpdatingLocation()
        }
    }
    
    @objc func stopUpdatingLocation() {
        clLocationManager.stopUpdatingLocation()
    }
    
    //MARK: Reverse geocoding
    func addressFromCoordinates( _ coordinates:(latitude: CLLocationDegrees, longitude: CLLocationDegrees),
                                 completionHandler: @escaping (String?) -> Void) {
        
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks,
                let placemark = placemarks.first {
                completionHandler(placemark.compactAddress)
            } else {
                if let error = error {
                    print("Unable to Reverse Geocode Location (\(error))")
                }
                completionHandler("")
            }
        }
    }
    
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lastLocation: CLLocation = locations[locations.count - 1]
        if let locationBlock = self.locationBlock {
            locationBlock(lastLocation.coordinate)
        }
    }
}
